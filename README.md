# Репозиторий Minoca OS 0.4 для изучения

Minoca OS is a general purpose operating system written from scratch. It aims to be lean, maintainable, modular, and compatible with existing software. It features a POSIX-like interface towards application software, and a growing suite of popular packages already built and ready to go. On the backend, it contains a powerful driver model between device drivers and the kernel. The driver model enables drivers to be written in a forward compatible manner, so that kernel level components can be upgraded without necessarily requiring a recompilation of all device drivers.

Minoca OS is event driven, preemptible, SMP ready, and network capable. It currently runs on x86 PCs and a range of ARM boards.

### Screenshots
![Installing Git on Minoca OS](docs/screenshots/Installing-Git.png)
![Memory Profiler](docs/screenshots/Memory-Profiler.png)
![Stack Profiler](docs/screenshots/Stack-Profiler.png)

### Getting Started
If you're just looking to try out Minoca OS, head over to our [download](https://www.minocacorp.com/download/) page to grab the latest stable images. The rest of this page describes how to use this repository to build your own custom image of Minoca OS.

## Building Minoca OS
The paragraphs below will get you from a fresh clone to a built image.

### Environment
The Minoca OS build environment is keyed off of a few environment variables you'll need to set in order to orient the build system:
 - `SRCROOT` - Contains the absolute path to the base source directory. This respository is expected to be in a directory called `os` inside `SRCROOT`. If the third-party or tools repositories are present, they should be in directories called `third-party` and `tools` respectively underneath `SRCROOT`. For example, if you had checked out this repository into `~/src/os`, then in your shell you'd run `export SRCROOT=~/src`.
 - `ARCH` - Contains the architecture to build Minoca OS for (aka the target architecture). Valid values are `armv6`, `armv7`, and `x86`.
 - `VARIANT` - Contains the architecture variant, if any. Leave this unset most of the time. Currently the only valid value is `q` for the `x86` architecture, which builds for the Intel Quark.
 - `DEBUG` - Describes whether to build Minoca OS for debugging or release. Valid values are `dbg` for debug or `rel` for release. We always build `dbg`.
 - `PATH` - You'll need to have `$SRCROOT/$ARCH$VARIANT$DEBUG/tools/bin` in your path to build successfully.

### Prerequisites
To build Minoca OS you'll need a Minoca-specific toolchain for the particular architecture you're building. Prebuilt toolchains can be found [here](https://www.minocacorp.com/download/#toolchain). If you want to build the toolchain from sources, you'll need to check out the [third-party](https://gitlab.com/minoca/third-party) repository and run "make tools" in there.
> Note: If you want to build your own toolchain on Windows, you may find the [tools](https://gitlab.com/minoca/tools) repository helpful, as it contains a native MinGW compiler, make, and other tools needed to bootstrap a toolchain on Windows.

### Build
Run `make` to build the OS for the particular architecture you've supplied. Parallel make is supported. The final output of the build will be several .img files located in `$SRCROOT/$ARCH$VARIANT$DEBUG/bin/*.img`. For example, the PC image is usually located at `$SRCROOT/x86dbg/bin/pc.img`. This is a raw hard disk file that can be applied directly to a hard drive or USB stick to boot Minoca OS. The image `install.img` is a generic installation archive that the `msetup` tool can use to create new Minoca OS installations on target disks or partitions.

Object files are generated in `$SRCROOT/$ARCH$VARIANT$DEBUG/obj/os`. You can run `make clean`, or simply delete this directory, to cause the os repository to completely rebuild. Alternatively, you can run `make wipe` to delete all generated files, including the third-party tools you built or downloaded. Running `make wipe` simply deletes `$SRCROOT/$ARCH$VARIANT$DEBUG/`. We usually stick to `make clean` since `make wipe` requires a complete rebuild of the toolchain.

A note for macOS users: We've managed to build successfully using both GCC from XCode 8 (really clang) and Homebrew GCC, both using the 10.12 SDK. Some users have reported that they need to export `SDKROOT=$(xcrun --show-sdk-path)` to build properly.

### Running
To boot your built images, you can write the appropriate image for the platform you're trying to boot to a USB flash drive or hard disk. On Windows, you can use the Win32DiskImager tool (included in the [tools](https://gitlab.com/minoca/tools) repository under win32/Win32DiskImager). You can also use the msetup tool to build custom images. If you use the msetup tool to install Minoca OS onto a partition of a disk containing other partitions that you care about (such as on the same machine you're building from), we highly recommend making a complete backup of your disk. Minoca OS is still new, and we wouldn't want a bad bug to wipe out all your data.

If you're building Minoca OS on Windows and have downloaded the [tools](https://gitlab.com/minoca/tools) repository, several shortcuts have been set up to allow you to quickly run a Qemu instance with the images you've just built. Make sure you fired up the development environment with the setenv.cmd script. Type `run`, then `dx` to fire up an x86 Qemu instance of pc.img with a kernel debugger attached. We use this internally for faster development. If building for ARM, it's `runarm` and `da`.

### Структура исходного кода
Ниже приведено общее представление о том, что находится в репозитории. Для более детального представления о содержимом директорий проверяйте файл Makefile в соответствующей директории.

 * `apps` - Приложения и библиотеки, работающие в пространстве посльзователя (User mode)
   * `ck` - Язык сценариев для встраиваемых устройств Chalk
   * `debug` - Приложение отладки
   * `libc` - Библиотека языка C для ОС Minoca
   * `osbase` - Библиотека с API ядра ОС Minoca
   * `setup` - Сборочная утилита msetup
   * `swiss` - Утилиты POSIX в приятной упаковке
 * `boot` - Исполняемые файлы, используемые в процессе загрузки ОС
   * `mbr` - Главная загрузочная запись
   * `fatboot` - Загрузочная запись разделов с использованием файловой системы FAT
   * `bootman` - Менеджер загрузки ОС Minoca
   * `loader` - Загрузчик ОС Minoca
   * `lib` - Разделямые библиотеки, используемые множеством загрузочных исполняемых файлов
 * `drivers` - Драйверы устройств
   * `acpi` - Драйвер платформы ACPI с интерпретатором языка AML
   * `fat` - Драйвер ФС FAT
   * `gpio` - Ядро библиотеки ввода-вывода общего назначения (GPIO) и драйверы для систем на кристалле
   * `net` - Поддержка сети
     * `ethernet` - Драйверы для поддержки сетевых контроллеров на базе витой пары
     * `net80211` - Основная поддержка сети протокола 802.11
     * `netcore` - Основная поддержка сети (TCP, UDP, IP, ARP и др.)
     * `wireless` - Драйвера контроллеров беспроводной сети протокола 802.11
   * `pci` - Поддержка шины PCI
   * `sd` - Поддержка флеш-карт SD/MMC
   * `spb` - Драйверы шины последовательного периферийного интерфейса SPB (I2C, SPI)
   * `special` - Специальные устройства (/dev/null, full, zero)
   * `usb` - Поддержка USB
     * `ehci` - Поддержка хост-контроллера EHCI
     * `usbcomp` - Поддержка составных USB-устройств
     * `usbhid` - Поддержка USB HID
     * `usbhub` - Поддержка хаба USB
     * `usbkbd` - Поддержка USB-клавиатур
     * `usbmass` - Поддержка USB-накопителей
     * `usbmouse` - Поддержка USB-мышей
   * `input` - Драйверы ввода в пользовательском пространстве
   * `videocon` - Драйвер видео-терминал-консоли
 * `images` - Рецепты для создания финальных образов для каждой поддерживаемой платформы
 * `include` - Заголовочные файлы, доступные публично
 * `kernel` - Ядро ОС Minoca
   * `ke` - Исполняемые функции высокого уровня
   * `mm` - Управление памятью
   * `io` - Подсистема ввода-вывода
   * `kd` - Поддержка отладки ядра
   * `hl` - Поддержка низко-уровневого слоя для работы с аппаратным обеспечением
   * `ob` - Управление объектами
   * `ps` - Управление процессами и тредами
   * `sp` - Поддержка системного профилировщика
 * `lib` - Общие библиотеки, используемые в процессе загрузки, в пространствах ядра и пользователя
   * `basevid` - Библиотека для отрисовки текста через фреймбуфер
   * `fatlib` - Библиотека файловой системы FAT
   * `im` - Библиотека образа ELF/PE
   * `partlib` - Библиотека разделов
   * `rtl` - Библиотека с основными функциями, используемыми в ходе выполнения программ (printf, дата/время, memcpy, и др.)
   * `termlib` - Библиотека поддержки терминала
 * `tasks` - Конфигурация автоматической сборки
 * `uefi` - Минимальная имплементация UEFI для платформ, поддерживаемых ОС Minoca.
   * `core` - Платформо-агностический основной набор прошивок UEFI
   * `dev` - Библиотеки устройств UEFI
   * `plat` - Сценарии сборки и коды прошивок для различных платформ
     * `beagbone` - Прошивка для BeagleBone Black
     * `bios` - Прошивка для работы UEFI поверх BIOS'а
     * `integcp` - Прошивка для Integrator/CP (ARM Qemu)
     * `panda` - Прошивка для TI PandaBoard
     * `rpi` - Прошивка для Raspberry Pi 1
     * `rpi2` - Прошивка для Raspberry Pi 2 и 3
     * `veyron` - Прошивка для Asus C201 Chromebook
   * `tools` - Утилиты, используемые для финальной сборки образов для прошивок

## Contributing
Submissions are welcome! See our [CONTRIBUTING.md](CONTRIBUTING.md) page for details, or our [WISHLIST.md](WISHLIST.md) page for suggestions. Bugs can be reported here on Github.

## License
Minoca OS is licensed to the public under the terms of the GNU General Public License, version 3. Alternate licensing options are available. Contact info@minocacorp.com if your company is interested in licensing Minoca OS. For complete licensing information, see the [LICENSE](LICENSE) file in this repository.

## Contact
 * Email: minoca-dev@googlegroups.com
   * Contact security@minocacorp.com for security related issues.
   * Contact info@minocacorp.com for private or business inquries.
 * Website: [http://www.minocacorp.com/](http://www.minocacorp.com)
 * Github: [https://github.com/minoca](https://github.com/minoca)
 * Gitlab: [https://gitlab.com/minoca](https://gitlab.com/minoca)
 * IRC: [ircs://irc.oftc.net:6697/minoca-os](ircs://irc.oftc.net:6697/minoca-os)

## Дисклеймер
Данный репозиторий создан с целью перевода комментариев и документации на русский язык, а также создания обучающих материалов на русском языке по теме "Проектирование ОС" как части тематической линии "Информационные технологии" по М.Лапчику. 
Следите за обновлениями.
